"use strict";
function create(processor) {
    console.log(`
        processor berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${processor.brand}
        nama model: ${processor.modelName}
        nama base model: ${processor.baseModel}
        clock speed :  ${processor.clockSpeed}
    `);
}
function createIntel(intel) {
    console.log(`
        processor INTEL berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${intel.brand}
        nama model: ${intel.modelName}
        nama base model: ${intel.baseModel}
        clock speed :  ${intel.clockSpeed}
        turbo boost :  ${intel.turboBoost}
        series :  ${intel.series}
    `);
}
function createAMD(amd) {
    console.log(`
        processor AMD berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${amd.brand}
        nama model: ${amd.modelName}
        nama base model: ${amd.baseModel}
        clock speed :  ${amd.clockSpeed}
        precision boost :  ${amd.precisionBoost}
        series :  ${amd.series}
    `);
}
const processorGlobal = {
    brand: "intel",
    baseModel: "core i5",
    modelName: "i5-12000F",
    clockSpeed: 4,
};
const processorIntel = Object.assign(Object.assign({}, processorGlobal), { turboBoost: true, series: 9 });
const processorAMD = Object.assign(Object.assign({}, processorGlobal), { precisionBoost: true, series: 7 });
create(processorGlobal);
createIntel(processorIntel);
createAMD(processorAMD);
