function create1(): number { // kalo number returnnya harus number
    return 300;
}

const create2 = () : string => "hallo"

function create3(): void { //fungsi void tidak mereturn apapun
    let a:number = 1;
    let b:number = 4;
    let jumlah:number = a+b
    console.log({jumlah})
}

function jumlah(a:number, b:number): void { //fungsi void tidak mereturn apapun
    let hasil:number = a+b
    console.log({hasil})
}

create3()
jumlah(10,30)
let result:any = create1()
console.log({result})