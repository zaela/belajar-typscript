// TIPE DATA CUSTOM
type TemanType = {
    nama:string, //wajib diisi
    isGood?:boolean, // ditambah tanda ? berarti tidak wajib diisi
}

type wanita = string;
type pria = number;
type Gender = wanita | pria

let jenisKelamin:Gender;
jenisKelamin = 'laki laki'

let temanKita:TemanType;

temanKita= {
    nama:"agus",
    isGood:true
}

console.log({temanKita})