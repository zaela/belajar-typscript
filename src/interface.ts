interface IProcessor {
    brand: string;
    baseModel: string;
    modelName: string;
    clockSpeed: number;
}

interface Intel extends IProcessor {
    turboBoost:boolean;
    series:3|5|7|9;
}

interface AMD extends IProcessor {
    precisionBoost:boolean;
    series:3|5|7;
}

function create(processor:IProcessor): void{
    console.log(`
        processor berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${processor.brand}
        nama model: ${processor.modelName}
        nama base model: ${processor.baseModel}
        clock speed :  ${processor.clockSpeed}
    `)
}

function createIntel(intel:Intel): void{
    console.log(`
        processor INTEL berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${intel.brand}
        nama model: ${intel.modelName}
        nama base model: ${intel.baseModel}
        clock speed :  ${intel.clockSpeed}
        turbo boost :  ${intel.turboBoost}
        series :  ${intel.series}
    `)
}

function createAMD(amd:AMD): void{
    console.log(`
        processor AMD berhasil dibuat dengan spesifikasi sebgai berikut:
        brand: ${amd.brand}
        nama model: ${amd.modelName}
        nama base model: ${amd.baseModel}
        clock speed :  ${amd.clockSpeed}
        precision boost :  ${amd.precisionBoost}
        series :  ${amd.series}
    `)
}

const processorGlobal = {
    brand:"intel",
    baseModel:"core i5",
    modelName:"i5-12000F",
    clockSpeed:4,
}

const processorIntel: Intel = {
    ...processorGlobal,
    turboBoost:true,
    series:9,
}

const processorAMD: AMD = {
    ...processorGlobal,
    precisionBoost:true,
    series:7,
}

create(processorGlobal)
createIntel(processorIntel)
createAMD(processorAMD)